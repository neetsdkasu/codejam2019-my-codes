package main

import (
	"bufio"
	"fmt"
	"os"
)

var scanner = bufio.NewScanner(os.Stdin)

func init() {
	const size = 1e7
	scanner.Buffer(make([]byte, size), size)
}

func scan(args ...interface{}) {
	scanner.Scan()
	s := scanner.Text()
	fmt.Sscan(s, args...)
}

type Data struct{ x, y, d int }

func dir(d string) int {
	switch d {
	case "N":
		return 0
	case "S":
		return 1
	case "W":
		return 2
	default:
		return 3
	}
}

func main() {
	var t int
	scan(&t)

	for i := 1; i <= t; i++ {
		fmt.Printf("Case #%d: ", i)

		var p, q int
		scan(&p, &q)
		ds := make([]*Data, p)
		ct := make([][]int, q+1)
		for j := range ct {
			ct[j] = make([]int, q+1)
		}
		for j := 0; j < p; j++ {
			var s string
			d := &Data{}
			scan(&d.x, &d.y, &s)
			d.d = dir(s)
			ds[j] = d
			switch d.d {
			case 0:
				for y := d.y + 1; y <= q; y++ {
					for x := 0; x <= q; x++ {
						ct[y][x]++
					}
				}
			case 1:
				for y := d.y - 1; y >= 0; y-- {
					for x := 0; x <= q; x++ {
						ct[y][x]++
					}
				}
			case 2:
				for x := 0; x < d.x; x++ {
					for y := 0; y <= q; y++ {
						ct[y][x]++
					}
				}
			case 3:
				for x := d.x + 1; x <= q; x++ {
					for y := 0; y <= q; y++ {
						ct[y][x]++
					}
				}
			}

		}
		ansX, ansY, ansC := 0, 0, 0
		for x := 0; x <= q; x++ {
			for y := 0; y <= q; y++ {
				if c := ct[y][x]; c > ansC {
					ansX, ansY, ansC = x, y, c
				}
			}
		}
		fmt.Println(ansX, ansY)
	}

}
