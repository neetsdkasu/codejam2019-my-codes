package main

import (
	"bufio"
	"fmt"
	"os"
)

var scanner = bufio.NewScanner(os.Stdin)

func init() {
	const size = 1e7
	scanner.Buffer(make([]byte, size), size)
}

func scan(args ...interface{}) {
	scanner.Scan()
	s := scanner.Text()
	fmt.Sscan(s, args...)
}

func main() {
	var t, w int
	scan(&t, &w)

	out := bufio.NewWriter(os.Stdout)
	const MOD = 1 << 63
	for i := 1; i <= t; i++ {
		var v int
		var n, sum uint64
		rs := make([]uint64, 6)
		for j := 1; j <= w; j++ {
			println(j)
			ts := append([]uint64{}, rs...)
			for e := 1; e <= j; e++ {
				for x := 1; x <= 6; x++ {
					if e%x == 0 {
						ts[x-1] += ts[x-1]
						ts[x-1] %= MOD
					}
				}
			}
			sum = 0
			for _, tc := range ts {
				sum += tc
				sum %= MOD
			}
			println(sum)
			fmt.Fprintln(out, j)
			out.Flush()
			scan(&n)
			println(n)
			if n < 0 {
				return
			}
			n -= sum
			for e := 1; e < j; e++ {
				n /= 2
			}
			rs[j-1] = n
			println(n)
		}
		fmt.Fprintln(out, rs[0], rs[1], rs[2], rs[3], rs[4], rs[5])
		out.Flush()
		scan(&v)
		if v < 0 {
			break
		}
	}

}
