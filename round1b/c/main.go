package main

import (
	"bufio"
	"fmt"
	"os"
)

var scanner = bufio.NewScanner(os.Stdin)

func init() {
	const size = 1e7
	scanner.Buffer(make([]byte, size), size)
}

func scan(args ...interface{}) {
	scanner.Scan()
	s := scanner.Text()
	fmt.Sscan(s, args...)
}

func abs(a int) int {
	if a < 0 {
		return -a
	} else {
		return a
	}
}

func max(a, b int) int {
	if a > b {
		return a
	} else {
		return b
	}
}

func main() {
	var t int
	scan(&t)

	for i := 1; i <= t; i++ {
		fmt.Printf("Case #%d: ", i)

		var n, k int
		scan(&n, &k)
		cs := make([]int, n)
		ds := make([]int, n)
		red := make([]interface{}, n)
		for j := range cs {
			red[j] = &cs[j]
		}
		scan(red...)
		for j := range ds {
			red[j] = &ds[j]
		}
		scan(red...)
		ans := 0
		for j := 0; j < n; j++ {
			c, d := 0, 0
			for e := j; e >= 0; e-- {
				c, d = max(c, cs[e]), max(d, ds[e])
				p := abs(c - d)
				if p <= k {
					ans++
				}
			}
		}
		fmt.Println(ans)
	}

}
