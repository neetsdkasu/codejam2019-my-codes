package main

import (
	"bufio"
	"fmt"
	"os"
)

var scanner = bufio.NewScanner(os.Stdin)

func init() {
	const size = 1e7
	scanner.Buffer(make([]byte, size), size)
}

func scan(args ...interface{}) {
	scanner.Scan()
	s := scanner.Text()
	fmt.Sscan(s, args...)
}

func main() {
	var t int
	scan(&t)

	for i := 1; i <= t; i++ {
		fmt.Printf("Case #%d: ", i)

		var n int
		var p string
		scan(&n)
		scan(&p)
		pp := []byte(p)
		ans := make([]byte, 0, len(pp))
		for _, ch := range pp {
			if ch == 'E' {
				ans = append(ans, 'S')
			} else {
				ans = append(ans, 'E')
			}
		}
		fmt.Println(string(ans))
	}

}
