package main

import (
	"bufio"
	"fmt"
	"os"
)

var scanner = bufio.NewScanner(os.Stdin)

func init() {
	const size = 1e7
	scanner.Buffer(make([]byte, size), size)
}

func scan(args ...interface{}) {
	scanner.Scan()
	s := scanner.Text()
	fmt.Sscan(s, args...)
}

func main() {
	var t int
	scan(&t)

	for i := 1; i <= t; i++ {
		fmt.Printf("Case #%d: ", i)

		var n string
		scan(&n)
		s := []byte(n)
		ans1 := make([]byte, 0, len(s))
		ans2 := make([]byte, 0, len(s))
		for _, ch := range s {
			if ch == '4' {
				ans1 = append(ans1, '3')
				ans2 = append(ans2, '1')
			} else {
				ans1 = append(ans1, ch)
				if len(ans2) > 0 {
					ans2 = append(ans2, '0')
				}
			}
		}
		if len(ans2) == 0 {
			ans2 = append(ans2, '0')
		}
		fmt.Println(string(ans1), string(ans2))
	}

}
