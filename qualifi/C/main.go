package main

import (
	"bufio"
	"fmt"
	"math/big"
	"os"
	"sort"
)

var scanner = bufio.NewScanner(os.Stdin)

func init() {
	const size = 1e7
	scanner.Buffer(make([]byte, size), size)
}

func scan(args ...interface{}) {
	scanner.Scan()
	s := scanner.Text()
	fmt.Sscan(s, args...)
}

type BigIntSlice []*big.Int

func (s BigIntSlice) Len() int {
	return len(s)
}

func (s BigIntSlice) Less(i, j int) bool {
	return s[i].Cmp(s[j]) < 0
}

func (s BigIntSlice) Swap(i, j int) {
	s[i], s[j] = s[j], s[i]
}

func main() {
	var t int
	scan(&t)

	for i := 1; i <= t; i++ {
		fmt.Printf("Case #%d: ", i)

		var n string
		var l int
		scan(&n, &l)
		// fmt.Println(n, l)
		ss := make([]string, l)
		{
			refs := make([]interface{}, l)
			for j := range ss {
				refs[j] = &ss[j]
			}
			scan(refs...)
		}
		bs := make([]*big.Int, len(ss))
		for j, s := range ss {
			b := new(big.Int)
			b.SetString(s, 10)
			bs[j] = b
		}
		mp := make(map[string]*big.Int)
		res := make([]*big.Int, l+1)
		for j := range res {
			res[j] = nil
		}
		corner := false
		for j := 1; j < len(bs); j++ {
			g := new(big.Int)
			if res[j-1] == nil {
				g.GCD(nil, nil, bs[j], bs[j-1])
				if (corner || j == 1) && g.Cmp(bs[j]) == 0 {
					corner = true
					continue
				}
			} else {
				g.Div(bs[j-1], res[j-1])
			}
			mp[g.String()] = g
			if j == 1 {
				res[0] = new(big.Int)
				tmp := res[0].Div(bs[0], g)
				mp[tmp.String()] = tmp
			} else if j+1 == len(bs) {
				res[j+1] = new(big.Int)
				tmp := res[j+1].Div(bs[j], g)
				mp[tmp.String()] = tmp
			}
			res[j] = g
			if corner {
				corner = false
				for k := j; k > 0; k-- {
					res[k-1] = new(big.Int)
					tmp := res[k-1].Div(bs[k-1], res[k])
					mp[tmp.String()] = tmp
				}
			}
		}
		ps := make([]*big.Int, 0, 30)
		for _, p := range mp {
			ps = append(ps, p)
		}
		sort.Sort(BigIntSlice(ps))
		ans := make([]byte, 0, l+1)
		for _, r := range res {
			for j, p := range ps {
				if p.Cmp(r) == 0 {
					ans = append(ans, byte(j)+'A')
					break
				}
			}
		}
		fmt.Println(string(ans))
	}

}
