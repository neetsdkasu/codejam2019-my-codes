package main

import (
	"bufio"
	"fmt"
	"os"
)

var scanner = bufio.NewScanner(os.Stdin)

func init() {
	const size = 1e7
	scanner.Buffer(make([]byte, size), size)
}

func scan(args ...interface{}) {
	scanner.Scan()
	s := scanner.Text()
	fmt.Sscan(s, args...)
}

type Area struct {
	n, b int
}

func main() {
	var t int
	scan(&t)
	// print(fmt.Sprintln("t:", t))

	out := bufio.NewWriter(os.Stdout)

	for i := 1; i <= t; i++ {
		// print(fmt.Sprintln("i:", i))
		var n, b, f int
		scan(&n, &b, &f)
		// print(fmt.Sprintln("n", n, ", b", b, ", f", f))
		tmp := []*Area{}
		if n > 16 {
			query := make([]byte, 0, n)
			for j := 0; j < n; j++ {
				if (j/16)%2 == 0 {
					query = append(query, '1')
				} else {
					query = append(query, '0')
				}
			}
			fmt.Fprintln(out, string(query))
			out.Flush()
			var res string
			scan(&res)
			if "-1" == res {
				println("Wrong!")
				return
			}
			bs := []byte(res)
			cnt := 0
			tp := byte('1')
			sz := 0
			for _, ch := range bs {
				if tp == ch {
					cnt++
				} else {
					if sz+16 > n {
						tmp = append(tmp, &Area{n - sz, cnt})
					} else {
						tmp = append(tmp, &Area{16, cnt})
					}
					sz += 16
					cnt = 1
					tp ^= '1' ^ '0'
				}
			}
			if sz < n {
				if sz+16 > n {
					tmp = append(tmp, &Area{n - sz, cnt})
				} else {
					tmp = append(tmp, &Area{16, cnt})
				}
			}
			f--
		} else {
			tmp = append(tmp, &Area{n, n - b})
		}
		for q := 0; q < f; q++ {
			// print(fmt.Sprintln("q:", q))
			query := make([]byte, 0, n)
			for _, e := range tmp {
				for j := 0; j < e.n/2; j++ {
					query = append(query, '1')
				}
				for j := 0; j < e.n-e.n/2; j++ {
					query = append(query, '0')
				}
			}
			fmt.Fprintln(out, string(query))
			out.Flush()
			// print(fmt.Sprintln("query:", string(query)))
			var res string
			scan(&res)
			if "-1" == res {
				println("wrong!")
				return
			}
			// print(fmt.Sprintln("res", res))
			bs := []byte(res)
			tmp2 := []*Area{}
			for _, e := range tmp {
				if e.b == 0 {
					tmp2 = append(tmp2, e)
					continue
				}
				w := bs[:e.b]
				bs = bs[e.b:]
				ones := 0
				for _, ch := range w {
					if ch == '1' {
						ones++
					}
				}
				tmp2 = append(tmp2, &Area{e.n / 2, ones})
				tmp2 = append(tmp2, &Area{e.n - e.n/2, e.b - ones})
			}
			tmp = tmp2
		}
		ans := make([]interface{}, 0, b)
		idx := 0
		for _, e := range tmp {
			for j := 0; j < e.n-e.b; j++ {
				ans = append(ans, idx+j)
			}
			idx += e.n
		}
		fmt.Fprintln(out, ans...)
		out.Flush()
		// print("ans: " + fmt.Sprintln(ans...))
		var es string
		scan(&es)
		if "1" != es {
			println(es)
			println("WRONG!")
			return
		}
	}

}
