CodeJam2019 My Codes
====================


https://codingcompetitions.withgoogle.com/codejam  



### Qualification Round

Rank: 941    
Score: 100  
Penalty: 1  
A ... set1: 6pt, set2: 10pt, set3: 1pt  (1/0) (12:08:14)  
B ... set1: 5pt, set2: 9pt, set3: 10pt  (1/0) (12:18:59)  
C ... set1: 10pt, set2: 15pt  (1/0) (18:44:18)  
D ... set1: 14pt, set2: 20pt  (2/1) (24:29:54)  


https://codingcompetitions.withgoogle.com/codejam/round/0000000000051705  




### Round 1B

Rank: 4478  
Score: 23  
Penarlty: 0  
A ... set1: 9pt, set2: 0pt  (1/0) (00:50:18)  
B ... set1: 0pt, set2: 0pt  (3/0)  
C ... set1: 14pt, set2: 0pt (1/0) (01:16:05)  


https://codingcompetitions.withgoogle.com/codejam/round/0000000000051706  



### Round 1C

Rank: 3548  
Score: 28  
Penarlty: 4  
A ... set1: 10pt, set2: 18pt  (7/4) (02:21:36)  
B ... set1: 0pt, set2: 0pt  (0/0)  
C ... set1: 0pt, set2: 0pt (0/0)  


https://codingcompetitions.withgoogle.com/codejam/round/00000000000516b9  



