package main

import (
	"bufio"
	"fmt"
	"os"
)

var scanner = bufio.NewScanner(os.Stdin)

func init() {
	const size = 1e7
	scanner.Buffer(make([]byte, size), size)
}

func scan(args ...interface{}) {
	scanner.Scan()
	s := scanner.Text()
	fmt.Sscan(s, args...)
}

func main() {
	var t int
	scan(&t)

	for ti := 1; ti <= t; ti++ {
		fmt.Printf("Case #%d: ", ti)

		var a int
		scan(&a)
		flag := make([][3]int, 500)
		css := [][]byte{}
		for i := 0; i < a; i++ {
			var cs string
			scan(&cs)
			c := []byte(cs)
			css = append(css, c)
			k := 0
			for j := range flag {
				switch c[k] {
				case 'R':
					flag[j][0]++
				case 'P':
					flag[j][1]++
				case 'S':
					flag[j][2]++
				}
				k++
				if k >= len(c) {
					k = 0
				}
			}
		}
		lost := make([]bool, a)
		ans := []byte{}
		win := false
	outerloop:
		for j, prs := range flag {
			e := 0
			if prs[0] > 0 {
				e |= 1
			}
			if prs[1] > 0 {
				e |= 2
			}
			if prs[2] > 0 {
				e |= 4
			}
			q := byte('P')
			switch e {
			case 7:
				break outerloop
			case 1:
				ans = append(ans, 'P')
				win = true
				break outerloop
			case 2:
				ans = append(ans, 'S')
				win = true
				break outerloop
			case 4:
				ans = append(ans, 'R')
				win = true
				break outerloop
			case 3:
				ans = append(ans, 'P')
				q = 'R'
			case 5:
				ans = append(ans, 'R')
				q = 'S'
			case 6:
				ans = append(ans, 'S')
				q = 'P'
			default:
				break outerloop
			}
			for i, c := range css {
				if lost[i] {
					continue
				}
				k := j % len(c)
				if c[k] != q {
					continue
				}
				lost[i] = true
				for z := j; z < len(flag); z++ {
					k = z % len(c)
					switch c[k] {
					case 'R':
						flag[z][0]--
					case 'P':
						flag[z][1]--
					case 'S':
						flag[z][2]--
					}
				}
			}
		}

		if win {
			fmt.Println(string(ans))
		} else {
			fmt.Println("IMPOSSIBLE")
		}

	}

}
